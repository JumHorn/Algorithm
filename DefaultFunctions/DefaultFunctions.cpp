#include<iostream>
using namespace std;

class Dog
{
public:
	explicit Dog()
	{
		cout << "ctor" << endl;
		count++;
		cout << count << endl;
	}
	~Dog()
	{
		cout << "dtor" << endl;
		count--;
		cout << count << endl;
	}
	Dog(const Dog& other)
	{
		cout << "copy" << endl;
		count++;
		cout << count << endl;
	}
	Dog& operator=(const Dog& other)
	{
		cout << "assign" << endl;
		count++;
		cout << count << endl;
		return const_cast<Dog&>(other);
	}
private:
	static int count;
};

int Dog::count = 0;

const Dog& getDog(Dog d)
{
	return d;
}

Dog getDog()
{
	return Dog();
}

int main()
{
	Dog* d = NULL;
	delete d;
	//Dog D = getDog(d);
	//d = getDog(D);
	//d = getDog();
	return 0;
}