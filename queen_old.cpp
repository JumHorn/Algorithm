/*
 *	still not solved 
 *	stack overflow
 */
#include<iostream>
using namespace std;

class queen
{
private:
    int grid[8][8];
    int backtracing[8];
    int total;
public:
    queen()
    {
        total=0;
        memset((void *)grid,0,sizeof(grid));
        memset((void *)backtracing,0,sizeof(backtracing));
    }
    void find(int m,int n)
    {
        backtracing[m]=n;
        if(m==0&&n==8)
        {
            return;
        }
        if(n==8)
        {
           return find(m-1,backtracing[m-1]+1);
        }
		//对该点的纵向判断
        int i;
        for(i=0;i<m;i++)
        {
            if(backtracing[i]==n)
            {
                break;
            }
        }
        if(i==m)
        {
            if(m==7)
            {
                print();
                return find(m-1,backtracing[m-1]+1);
            }
            else
            {
                return find(m+1,0);
            }
        }
        else
        {
            return find(m,n+1);
        }
    }

    void print()
    {
        total++;
        memset((void *)grid,0,sizeof(grid));
        for(int i=0;i<8;i++)
        {
            grid[i][backtracing[i]]=1;
        }
        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {
                cout<<grid[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }

    int getTotal()
    {
        return total;
    }
};

int main()
{
    queen q;
    q.find(0,0);
    cout<<q.getTotal()<<endl;
}
