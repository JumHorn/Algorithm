#include<sys/socket.h>
#include <netinet/in.h>//for struct sockaddr_in
#include<iostream>
#include<arpa/inet.h>//for inet_addr()
using namespace std;

int main()
{
	int sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		cout<<"socket error"<<endl;
		return -1;
	}

	struct sockaddr_in dst;
	memset((void*)&dst,0,sizeof(dst));
	dst.sin_family=AF_INET;
	dst.sin_port=htons(8000);
	dst.sin_addr.s_addr=inet_addr("192.168.2.101");
	int i=0x00000041;
	sendto(sockfd,(char*)&i,4,0,(struct sockaddr*)&dst,sizeof(dst));
	return 0;
}
