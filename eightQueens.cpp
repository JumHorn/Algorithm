#include<iostream>
using namespace std;

class queen
{
private:
    int grid[8][8];//only for print
    int backtracing[8];//core algorithm data
    int total;//only for statistics
public:
    queen()
    {
        total=0;
        memset((void *)grid,0,sizeof(grid));
        memset((void *)backtracing,0,sizeof(backtracing));
    }
    void find(int m,int n)
    {
        backtracing[m]=n;
        if(m==0&&n==8)
        {
            return;
        }
        if(n==8)
        {
            return find(m-1,backtracing[m-1]+1);
        }

        //vertical check
        for (int i = 0; i < m; i++)
        {
            if (backtracing[i] == n)
            {
                return find(m, n + 1);
            }
        }

        //slant check
        int t;
        //check left
        t = m > n ? n : m;
        for (int j = 0; j < t; j++)
        {
            if (backtracing[m - j - 1] == n - j - 1)
            {
                return find(m, n + 1);
            }
        }

        //check right
        t = m > 7 - n ? 7 - n : m;
        for (int j = 0; j < t; j++)
        {
            if (backtracing[m - j - 1] == n + j + 1)
            {
                return find(m, n + 1);
            }
        }

        //success or not
        if (m == 7)
        {
            print();
            return find(m - 1, backtracing[m - 1] + 1);
        }
        else
        {
            return find(m + 1, 0);
        }
    }

    void print()
    {
        total++;
        memset((void *)grid,0,sizeof(grid));
        for(int i=0;i<8;i++)
        {
            grid[i][backtracing[i]]=1;
        }
        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {
                cout<<grid[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }

    int getTotal()
    {
        return total;
    }
};

int main()
{
    queen q;
    q.find(0,0);
    cout<<q.getTotal()<<endl;
}
