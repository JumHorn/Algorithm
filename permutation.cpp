#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

ostream& operator<<(ostream& os,vector<int>& v)
{
	for(int i=0;i<v.size();i++)
		os<<v[i]<<" ";
	return os<<endl;
}

void permutation(vector<int>& v,vector<int>& tmp,int start)
{
	cout<<tmp;
	for(vector<int>::size_type i=start;i<v.size();i++)
	{
		swap(v[start],v[i]);
		tmp.push_back(v[start]);
		permutation(v,tmp,start+1);
		tmp.pop_back();
		swap(v[start],v[i]);
	}
}

void permutation(vector<int>& v,vector<int>& tmp,int size,int start)
{
	if(tmp.size()==size)
	{
		cout<<tmp;
		return;
	}
	for(vector<int>::size_type i=start;i<v.size();i++)
	{
		swap(v[start],v[i]);
		tmp.push_back(v[start]);
		permutation(v,tmp,size,start+1);
		tmp.pop_back();
		swap(v[start],v[i]);
	}	
}

int main()
{
	int a[]={1,2,3,4};
	vector<int> v(a,a+4);
	vector<int> tmp;
	permutation(v,tmp,2,0);
	permutation(v,tmp,0);
	return 0;
}

