/*
a command to see these gcc macro
gcc -dM -E - </dev/null
*/


#include<iostream>
using namespace std;

int main()
{
#ifdef __APPLE__
const char newline = '\r';
#endif

#ifdef __LINUX__
const char newline = '\n';
#endif

#ifdef _WIN32
const char newline = '\n';
#endif
	cout<<(int)newline<<endl;
	return 0;
}
