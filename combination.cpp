#include<iostream>
#include<vector>
using namespace std;

ostream& operator<<(ostream& os,vector<int>& v)
{
	for(int i=0;i<v.size();i++)
		os<<v[i]<<" ";
	return os<<endl;
}

void combination(vector<int>& v,vector<int>& tmp,int start)
{
	cout<<tmp;
	for(vector<int>::size_type i=start;i<v.size();i++)
	{
		tmp.push_back(v[i]);
		combination(v,tmp,i+1);
		tmp.pop_back();
	}
}

void combination(vector<int>& v,vector<int>& tmp,int size,int start)
{
	if(tmp.size()==size)
	{
		cout<<tmp;
		return;
	}
	for(vector<int>::size_type i=start;i<v.size();i++)
	{
		tmp.push_back(v[i]);
		combination(v,tmp,size,i+1);
		tmp.pop_back();
	}	
}

int main()
{
	int a[]={1,2,3,4};
	vector<int> v(a,a+4);
	vector<int> tmp;
	combination(v,tmp,2,0);
	combination(v,tmp,0);
	return 0;
}
