#include<iostream>
#include<list>
#include<algorithm>
#include "Huffman.h"

struct Huffmantree
{
	Huffmantree(char v) :left(NULL), right(NULL), val(v) {}
	~Huffmantree()
	{
		if (left != NULL)
		{
			delete left;
		}
		if (right != NULL)
		{
			delete right;
		}
	}

	Huffmantree* left;
	Huffmantree* right;
	char val;
};

Huffman::Huffman() :tree(NULL), hash(256), decode(256, vector<int>(2))
{
}


Huffman::~Huffman()
{
	if (tree != NULL)
		delete tree;
}

void Huffman::createTree()
{
	list<pair<int,int> > nodelist;
	list<Huffmantree*> treenode;
	for (int i = 0; i < (int)hash.size(); i++)
	{
		if (hash[i] != 0)
			nodelist.push_back(pair<int, int>(hash[i], i));
	}
	if (nodelist.empty())
		return;
	nodelist.sort(*this);
	for (list<pair<int, int> >::iterator it = nodelist.begin(); it != nodelist.end(); it++)
	{
		treenode.push_back(new Huffmantree(it->second));
	}
	while (!nodelist.empty())
	{
		if (nodelist.size() == 1)
		{
			tree = treenode.front();
			break;
		}
		list<pair<int, int> >::iterator tmp = nodelist.begin();
		int val1 = tmp->first;
		tmp++;
		int val2 = tmp->first;
		tmp++;
		nodelist.pop_front();
		nodelist.pop_front();
		int nodeval = val1 + val2;
		Huffmantree* node = new Huffmantree(0);
		list<Huffmantree*>::iterator tmpnode = treenode.begin();
		node->left = *tmpnode++;
		node->right = *tmpnode++;
		treenode.pop_front();
		treenode.pop_front();
		while (tmp != nodelist.end() && nodeval > tmp->first)
		{
			tmpnode++;
			tmp++;
		}
		nodelist.insert(tmp, pair<int, int>(nodeval, 0));
		treenode.insert(tmpnode, node);
	}
}

void Huffman::createCode()
{
	createCode(tree);
}

bool Huffman::operator()(const pair<int, int>& left, const pair<int, int>& right)
{
	if (left.first < right.first)
		return true;
	return false;
}

vector<char>& Huffman::encoding(const string& str)
{
	char tmp = '\0';
	int index = 0;
	for (string::const_iterator iter = str.begin(); iter != str.end(); iter++)
	{
		for (int i = decode[*iter][1]-1; i >= 0; i--)
		{
			tmp = (tmp | (((decode[*iter][0] >> i) & 1) << index));
			if (++index == 8)
			{
				zip.push_back(tmp);
				tmp = '\0';
				index = 0;
			}
		}
	}
	if (index != 0)
	{
		zip.push_back(tmp);
	}
	return zip;
}

vector<char>& Huffman::decoding()
{
	int index = 0;
	Huffmantree* node = tree;
	for (vector<char>::iterator iter = zip.begin(); iter != zip.end(); iter++)
	{
		index = 0;
		while (index != 8)
		{
			int tmp = (((*iter) >> index) & 1);
			if (tmp == 0)
			{
				node = node->left;
			}
			else
			{
				node = node->right;
			}
			index++;
			if (node->left == NULL&&node->right == NULL)
			{
				src.push_back(node->val);
				node = tree;
			}
		}
	}
	return src;
}

//preorder traversal
string Huffman::serialize(Huffmantree * tree)
{
	if (tree == NULL)
	{
		return string();
	}
	string res = tree->val + to_string(tree->left ? 1 : 0) + to_string(tree->right ? 1 : 0);
	string left = serialize(tree->left);
	string right = serialize(tree->right);
	res += left + right;
	return res;
}

Huffmantree * Huffman::deserialize(string& serial)
{
	int tmp = 0;
	return deserialize(serial, tmp);
}

void Huffman::createCode(Huffmantree* tree, int code, int num)
{
	if (tree == NULL)
	{
		return;
	}
	if (tree->left == NULL&&tree->right == NULL)
	{
		decode[tree->val][0] = code;
		decode[tree->val][1] = num;
	}
	createCode(tree->left, code << 1, num + 1);
	createCode(tree->right, (code << 1) | 1, num + 1);
}

Huffmantree * Huffman::deserialize(string & serial, int& index)
{
	if (index == serial.size())
		return NULL;
	int tmp = index;
	Huffmantree* root = new Huffmantree(serial[tmp]);
	if (serial[tmp + 1] == '1')
	{
		index = index + 3;
		root->left = deserialize(serial, index);
	}
	if (serial[tmp + 2] == '1')
	{
		index = index + 3;
		root->right = deserialize(serial, index);
	}
	return root;
}

/*
can be invoked many times
*/
void Huffman::calcFreq(const string& str)
{
	for (string::const_iterator iter = str.begin(); iter != str.end();iter++)
	{
		hash[*iter]++;
	}
}

ostream& operator<<(ostream& os, Huffman& h)
{
	for (int i = 0; i < (int)h.decode.size(); i++)
	{
		if (h.decode[i][1] != 0)
			os << (char)i << ": " << h.decode[i][0] << "," << h.decode[i][1] << endl;
	}
	return os;
}