#ifndef _HUFFMAN_H_
#define _HUFFMAN_H_

#include<vector>
#include<string>
using namespace std;

struct Huffmantree;

class Huffman
{
public:
	Huffman();
	~Huffman();

	//create charactor zip code
	void calcFreq(const string& str);
	void createTree();
	void createCode();
	bool operator()(const pair<int, int>& left, const pair<int, int>& right);

	//coding
	vector<char>& encoding(const string& str);
	vector<char>& decoding();

	//serialize
	string serialize(Huffmantree* tree);
	Huffmantree* deserialize(string& serial);

	//access
	Huffmantree* getTree() { return tree; }

#ifdef _DEBUG
	friend ostream& operator<<(ostream& os, Huffman& h);
#endif

private:
	void createCode(Huffmantree* tree, int code = 0, int num = 0);
	Huffmantree* deserialize(string& serial, int& index);

private:
	Huffmantree* tree;//save to file
	string serial;
	vector<int> hash;
	vector<vector<int> > decode;
	vector<char> zip;//save to file
	vector<char> src;
};

#endif