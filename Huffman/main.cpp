#include<iostream>
#include<vector>
#include"Huffman.h"
using namespace std;
/*
aabbbbc
c 00
a 01
b 1

理论存储 01011111 00
实际存储 11111010 00000000
*/
int main()
{
	Huffman h;
	h.calcFreq("aabbbbc");
	h.createTree();
	h.createCode();
	cout << h << endl;
	vector<char> v(h.encoding("aabbbbc"));
	for (int i = 0; i < (int)v.size(); i++)
	{
		cout << hex << (int)v[i] << " ";//低2位有效
	}
	cout << endl;
	vector<char> v1(h.decoding());
	for (int i = 0; i < (int)v1.size(); i++)
	{
		cout << v1[i] << " ";
	}
	cout << endl;
	string serial = h.serialize(h.getTree());
	cout << serial << endl;
	h.deserialize(serial);
}