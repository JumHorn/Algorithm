/*
the variadic parameter differs from gcc and msvc

gcc shuold be like the following

//retrieve the types
#define TYPEOF(x) DETAIL_TYPEOF(DETAIL_TYPEOF_PROBE x,)
#define DETAIL_TYPEOF(...) DETAIL_TYPEOF_HEAD(__VA_ARGS__)
#define DETAIL_TYPEOF_HEAD(x, ...) REM x
#define DETAIL_TYPEOF_PROBE(...) (__VA_ARGS__),
//strip off the type
#define STRIP(x) EAT x
//show the type without parenthsis
#define PAIR(x) REM x
*/

#pragma once
#include<type_traits>
#include<utility>
#include<boost/preprocessor.hpp>

#define REM(...) __VA_ARGS__
#define EAT(...)

//retrieve the types
#define TYPEOF(x) DETAIL_TYPEOF_INT(DETAIL_TYPEOF_PROBE x,)
#define DETAIL_TYPEOF_INT(...) DETAIL_TYPEOF_INT2((__VA_ARGS__))
#define DETAIL_TYPEOF_INT2(tuple) DETAIL_TYPEOF_HEAD tuple
//#define TYPEOF(x) DETAIL_TYPEOF(DETAIL_TYPEOF_PROBE x,)
#define DETAIL_TYPEOF(...) DETAIL_TYPEOF_HEAD(__VA_ARGS__)
#define DETAIL_TYPEOF_HEAD(x, ...) REM x
#define DETAIL_TYPEOF_PROBE(...) (__VA_ARGS__),
//strip off the type
#define STRIP(x) EAT x
//show the type without parenthsis
#define PAIR(x) REM x

//A helper metafunction for adding const to a type
template<typename M,typename T>
class make_const
{
public:
	typedef T type;
};

template<typename M, typename T>
class make_const<const M, T>
{
public:
	typedef typename std::add_const<T>::type type;
};

#define REFLECTABLE(...)\
static const int fields_n=BOOST_PP_VARIADIC_SIZE(__VA_ARGS__);\
friend struct reflector;\
template<int N,class Self>\
struct field_data{};\
BOOST_PP_SEQ_FOR_EACH_I(REFLECT_EACH,data,BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define REFLECT_EACH(r,data,i,x)\
PAIR(x);\
template<class Self>\
struct field_data<i,Self>\
{\
	Self& self;\
	field_data(Self& self):self(self){}\
	typename make_const<Self,TYPEOF(x)>::type& get()\
	{\
		return self.STRIP(x);\
	}\
	typename std::add_const<TYPEOF(x)>& get() const\
	{\
		return self.STRIP(x);\
	}\
	const char* name() const\
	{\
		return BOOST_PP_STRINGIZE(STRIP(x));\
	}\
}; \

struct reflector
{
	template<int N,typename T>
	static typename T::template field_data<N, T> get_field_data(T& x)
	{
		return typename T::template field_data<N, T>(x);
	}

	//get the number of fields
	template<typename T>
	struct fields
	{
		static const int n = T::fields_n;
	};
};

struct field_visitor
{
	template<typename C,typename Visitor,typename I>
	void operator()(C& c, Visitor v, I)
	{
		v(reflector::get_field_data<I::value>(c));
	}
};

namespace
{
	template<class C,class Visitor,std::size_t...Is>
	void visit_each(C& c, Visitor v, std::index_sequence<Is...>)
	{
		int dummy[] = { 0,(v(reflector::get_field_data<Is>(c),0))... };
		static_cast<void>(dummy);
	}
}

template<typename C,typename Visitor>
void visit_each(C& c, Visitor v)
{
	visit_each(c, v, std::make_index_sequence<reflector::fields<C>::n>{});
}