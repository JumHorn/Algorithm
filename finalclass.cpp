template<typename T>
class VBase
{
	friend T;
private:
	VBase(){}
	~VBase(){}
};

class Final : virtual public VBase<Final>
{};

class Derived : public Final
{};

int main()
{
	Final f;
	Derived d;
}
