#include<iostream>
#include<windows.h>
using namespace std;

class Dog
{
public:
	bool cute;
};

int main()
{
	//Dog *d = new Dog; //this is false don't inistialize member variable
	Dog *d = new Dog(); //this is true
						//when using gcc, these are all false
	if (d->cute)
		cout << "true" << endl;
	else
		cout << "false" << endl;
	return 0;
}