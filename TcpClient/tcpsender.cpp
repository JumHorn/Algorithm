#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread/mutex.hpp>
#include <iostream>
using namespace boost::asio;
using namespace boost::asio::ip;

io_context netio;
tcp::socket sendsocket(netio);
char msg[] = "hello world";
char msg1[] = "Dog";
char msg2[] = "Cat";
char msg3[] = "Animal";
char recvmsg[15] = { 0 };

void handleError(boost::system::error_code)
{}

void handleReceive(const boost::system::error_code & error, std::size_t bytes_transferred)
{}

void handleClientRead(const boost::system::error_code & error, std::size_t bytes_transferred)
{
	std::cout << msg << std::endl;
	async_write(sendsocket, buffer(msg), handleClientRead);
}

int main()
{
	try
	{
		sendsocket.connect(tcp::endpoint(make_address("127.0.0.1"), 8192));
		async_write(sendsocket, buffer(msg), handleClientRead);
		netio.run();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}