#ifndef _MEMORYPOOL_H_
#define _MEMORYPOOL_H_

#include<limits>
#define INITSIZE 1000

template<typename T,std::size_t N=sizeof(T)*1000>
class MemoryPool
{
public:
	typedef T value_type;

	typedef value_type* pointer;
	typedef const value_type* const_pointer;

	typedef value_type& reference;
	typedef const value_type& const_reference;

	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;

	//convert this type to MemoryPool<_Other>
	template<typename _Other>
	struct rebind
	{
		typedef MemoryPool<_Other> other;
	};

	pointer address(reference val) const
	{
		return &val;
	}

	const_pointer address(const_reference val) const
	{
		return &val;
	}

	//default construct
	MemoryPool():freedmem(NULL)
	{
		blockhead=reinterpret_cast<freelist*>(::operator new(INITSIZE*sizeof(value_type)));
		blocktail=reinterpret_cast<freelist*>(blockhead+INITSIZE*sizeof(value_type)+1);
	}

	~MemoryPool()
	{
		while(blockhead!=blocktail)
		{
			freelist* head=blockhead+1;
			::operator delete(reinterpret_cast<void*>(blockhead));
			blockhead=head;
		}
		while(freedmem!=NULL)
		{
			freelist* head=freedmem->next;
			::operator delete(reinterpret_cast<void*>(freedmem));
			freedmem=head;
		}
	}

	//copy construct
	MemoryPool(const MemoryPool<T>& other)
	{
		MemoryPool();
	}

	template<typename _Other>
	MemoryPool(const MemoryPool<_Other>& other)
	{
		MemoryPool();
	}

	template<typename _Other>
	MemoryPool<T>& operator=(const MemoryPool<_Other>& other)
	{
		return (*this);
	}

	//ignore hint
	pointer allocate(size_type count,const void* hint)
	{
		return allocate(count);
	}
	
	pointer allocate(size_type count=1)
	{
		void* ptr=NULL;
		if(freedmem!=NULL)
		{
			ptr=freedmem;
			freedmem=freedmem->next;
		}
		else
		{
			if(blockhead>=blocktail)
			{
				blockhead=reinterpret_cast<freelist*>(::operator new(N*sizeof(value_type)));
				blocktail=reinterpret_cast<freelist*>(blockhead+N*sizeof(value_type)+1);
			}
			ptr=reinterpret_cast<freelist*>(blockhead++);
		}
		return reinterpret_cast<pointer>(ptr);
	}

	void deallocate(pointer ptr,size_type count=1)
	{
		if(ptr!=NULL)
		{
			reinterpret_cast<freelist*>(ptr)->next=freedmem;
			freedmem=reinterpret_cast<freelist*>(ptr);
		}
	}
	
	void construct(pointer ptr) const
	{
		::new(ptr) value_type();
	}

	void construct(pointer ptr,const_reference val)
	{
		::new(ptr) value_type(val);
	}

	template<typename U>
	void destroy(U* ptr) const
	{
		if(ptr!=NULL)
		{
			ptr->~U();
		}
	}

	size_type max_size() const
	{
		return ((size_type)(-1)/sizeof(value_type));
	}

	pointer create()
	{
		pointer* ptr=allocate();
		construct(ptr);
		return reinterpret_cast<pointer>(ptr);
	}

	pointer create(const_reference val)
	{
		pointer* ptr=allocate();
		construct(ptr,val);
		return reinterpret_cast<pointer>(ptr);
	}

	void release(pointer ptr)
	{
		destroy(ptr);
		deallocate(ptr);
	}

private:
	union freelist
	{
		value_type val;
		freelist* next;
	};

	freelist* freedmem;
	freelist* blockhead;
	freelist* blocktail;
};

#endif
