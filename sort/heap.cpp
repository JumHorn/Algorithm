#include<vector>
#include<iostream>
using namespace std;

void pushheap(vector<int>::iterator start, vector<int>::iterator end)
{
	int index = distance(start, end);
	if (index <= 1)
		return;
	while (index != 0)
	{
		int i = (index - 1) / 2;
		if (*(start + index) >= *(start + i))
			break;
		int tmp = *(start + i);
		*(start + i) = *(start + index);
		*(start + index) = tmp;
		index = i;
	}
}

void popheap(vector<int>::iterator start, vector<int>::iterator end)
{
	int val = *start, index = 0, len = distance(start, end), hole = 2 * index + 2;
	if (len < 1)
		return;
	*start = *(end - 1);
	*(end - 1) = val;
	while (hole < len - 1)
	{
		if (*(start + hole) > *(start + hole - 1))
			--hole;
		if (*(start + index) <= *(start + hole))
			break;
		int tmp = *(start + index);
		*(start + index) = *(start + hole);
		*(start + hole) = tmp;
		index = hole;
		hole = 2 * index + 2;
	}
	if (hole == len - 1 && (*(start + hole - 1) < *(start + index)))
	{
		int tmp = *(start + index);
		*(start + index) = *(start + hole - 1);
		*(start + hole - 1) = tmp;
	}
}

void makeheap(vector<int>::iterator start, vector<int>::iterator end)
{
	vector<int>::iterator left = start;
	while (start != end)pushheap(left, start++);
}

int main()
{
	int a[] = { 9,8,7,6,5,4,3,2,1,0 };
	vector<int> tmp(a, a + 10);
	makeheap(tmp.begin(), tmp.end());
	for (vector<int>::iterator iter = tmp.end(); iter != tmp.begin(); --iter)
	{
		popheap(tmp.begin(), iter);
		cout << *(iter - 1) << endl;
	}
	for (auto n : tmp)
		cout << n << " ";
}