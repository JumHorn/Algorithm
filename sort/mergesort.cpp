#include<vector>
#include<iostream>
using namespace std;

void conquer(vector<int>& tmp, vector<int>::iterator left, vector<int>::iterator right)
{
	vector<int>::iterator mid = left + distance(left, right) / 2, d = tmp.begin(), m = mid, d1 = d, start = left, end = right;
	while (left != mid&&m != right)*d++ = (*left < *m) ? *left++ : *m++;
	while (left != mid)*d++ = *left++;
	while (m != right)*d++ = *m++;
	while (start != end)*start++ = *d1++;
}

void divide(vector<int>& tmp, vector<int>::iterator left, vector<int>::iterator right)
{
	if (distance(left, right) <= 1)
		return;
	int mid = distance(left, right) / 2;
	divide(tmp, left, left + mid);
	divide(tmp, left + mid, right);
	conquer(tmp, left, right);
}

//����ҿ�
void mergesort(vector<int>::iterator left, vector<int>::iterator right)
{
	vector<int> tmp(distance(left, right));
	divide(tmp, left, right);
}

//int main()
//{
//	int a[] = { 9,8,7,6,5,4,3,2,1,0 };
//	vector<int> tmp(a, a + 10);
//	mergesort(tmp.begin(), tmp.end());
//	for (auto n : tmp)
//		cout << n << " ";
//}