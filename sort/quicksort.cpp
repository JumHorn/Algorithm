#include<vector>
#include<iostream>
using namespace std;

//����ҿ�
template<typename T>
void quicksort(typename vector<T>::iterator left,typename vector<T>::iterator right)
{
	if (distance(left, right) <= 0)
		return;
	typename vector<T>::iterator l = left, r = right-1;
	int mid = *left;
	while (distance(l, r) > 0)
	{
		while (distance(l, r) > 0 && *r >= mid)
			--r;
		*l = *r;
		while (distance(l, r) > 0 && *l <= mid)
			++l;
		*r = *l;
	}
	*l = mid;
	quicksort<T>(left,l);
	quicksort<T>(l+1,right);
}