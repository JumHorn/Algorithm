#include<iostream>
#include<vector>
#include<boost/version.hpp>
#include<boost/algorithm/string.hpp>
using namespace std;

int main()
{
	cout<<BOOST_VERSION<<endl;
	cout<<BOOST_LIB_VERSION<<endl;
	string str="test0 test1";
	vector<string> res;
	boost::split(res,str,boost::is_any_of(" "));
	for(vector<string>::iterator iter=res.begin();iter!=res.end();iter++)
	{
		cout<<*iter<<endl;
	}
	return 0;
}
