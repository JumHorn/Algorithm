#include<iostream>
#include<string>
#include<vector>
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;

/*
string algorithm
the name of function ended without copy is in place algorithm or vice versa
*/

int main()
{
	//case conversion
	string s = "    hello world		";
	to_upper(s);
	cout << s << endl;
	to_lower(s);
	cout << s << endl;

	//trimming
	cout << trim_left_copy(s) << endl;
	cout << trim_right_copy(s) << endl;
	trim(s);
	cout << s << endl;

	//predicates
	cout << starts_with(s, "hello") << endl;
	cout << ends_with(s, "world") << endl;
	cout << istarts_with(s, "HEllo") << endl;
	cout << iends_with(s, "World") << endl;
	cout << contains(s, "h") << endl;
	cout << all(s, [](char a) {return true; }) << endl;

	//find
	cout << find_first(s, "he") << endl;
	//split
	vector<string> result;
	split(result, s, is_any_of(" "), token_compress_on);
	for (auto n : result)
		cout << n << " ";
}