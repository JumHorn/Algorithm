#include<vector>
using namespace std;

class DSU
{
public:
	DSU(int size):parent(size),rank(size)
	{
		for(int i=0;i<size;i++)
			parent[i]=i;
	}

	~DSU(){}

	int Find(int x)
	{
		if(parent[x]!=x)
			parent[x]=Find(parent[x]);
		return parent[x];
	}

	bool Union(int x,int y)
	{
		int xr=Find(x),yr=Find(y);
		if(xr==yr)
			return false;
		else if(rank[xr]<rank[yr])
			parent[xr]=yr;
		else if(rank[xr]>rank[yr])
			parent[yr]=xr;
		else
		{
			parent[yr]=xr;
			rank[xr]++;
		}
		return true;
	}

private:
	vector<int> parent;
	vector<int> rank;
};
