#include<iostream>
#include<stack>
using namespace std;

//defintion for a tree node
struct treenode
{
	int val;
	treenode* left;
	treenode* right;
	treenode(int x):val(x),left(NULL),right(NULL){}
};

class Tree
{
public:
	//创建树
	treenode* CreateTree();
	void PreOrderTraversal(treenode* root);
	void InOrderTraversal(treenode* root);
	void PostOrderTraversal(treenode* root);
private:
	treenode* head;
};

//创建树
//          1
//       2     3
//    4      5
//             6
treenode* Tree::CreateTree()
{
	treenode* head = new treenode(1);
	head->left = new treenode(2);
	head->right = new treenode(3);
	head->left->left = new treenode(4);
	head->right->left = new treenode(5);
	head->right->left->right = new treenode(6);
	Tree::head = head;
	return head;
}

void Tree::PreOrderTraversal(treenode* root)
{
	stack<treenode* > s;
	treenode* node = root;
	while(node||!s.empty())
	{
		//PreOrder processing
		cout<<node->val<<" ";

		s.push(node);
		node=node->left;
		while(!node&&!s.empty())
		{
			node = s.top();
			s.pop();
			node = node->right;
		}
	}
}

void Tree::InOrderTraversal(treenode* root)
{
	stack<treenode* > s;
	treenode* node = root;
	while(node||!s.empty())
	{	
		s.push(node);
		node=node->left;
		while(!node&&!s.empty())
		{
			node = s.top();
			//InOrder processing
			cout<<node->val<<" ";

			s.pop();
			node = node->right;
		}
		
	}
}

void Tree::PostOrderTraversal(treenode* root)
{
	stack<treenode* > s;
	treenode* node = root;
	treenode* pre = NULL;
	s.push(root);
	while(!s.empty())
	{
		node=s.top();
		if((!node->left&&!node->right)||(pre && (node->left==pre||node->right==pre)))
		{
			//PostOrderProcessing
			cout<<node->val<<" ";

			pre = node;
			s.pop();
		}
		else
		{
			if(node->right)
			{
				s.push(node->right);
			}
			if(node->left)
			{
				s.push(node->left);
			}
		}
	}
}

int main()
{
	Tree* t = new Tree();
	t->PreOrderTraversal(t->CreateTree());
	cout<<endl;
	t->InOrderTraversal(t->CreateTree());
	cout<<endl;
	t->PostOrderTraversal(t->CreateTree());
	cout<<endl;	
}
