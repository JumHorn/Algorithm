//
//  ParameterMatching.h
//  template
//
//  Created by Jum on 2017/11/12.
//  Copyright © 2017年 Jum. All rights reserved.
//

#ifndef ParameterMatching_H
#define ParameterMatching_H

#include <iostream>
using namespace std;

template <typename T>
T const& max(T &a,T &b)
{
    cout<<"T const& max(T &a,T &b)"<<endl;
    return a>b?a:b;
}

//没有参数，与上面的形成函数重载
template<typename T>
void max()
{
    T t;
    cout<<"void max()"<<endl;
}

template<typename T>
void max(T t)
{
    cout<<"void max(T t)"<<endl;
}

void max()
{
    cout<<"max"<<endl;
}

#endif /* ParameterMatching_H */
