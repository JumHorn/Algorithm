//
//  print.h
//  FirstMac
//
//  Created by Jum on 2017/8/20.
//  Copyright © 2017年 Jum. All rights reserved.
//

#ifndef print_h
#define print_h
#include <vector>
template<typename T>
void print(T& vec);

#endif /* print_h */
