//
//  ReturnTemplateType.cpp
//  template
//
//  Created by Jum on 2017/11/12.
//  Copyright © 2017年 Jum. All rights reserved.
//

#include "ReturnTemplateType.h"

int main()
{
    //cannot deduce matched call
//    test(4,4.2);
    test<int,double,double>(4,4.2);
    
    //只需要指定return type即可
    test<int>(4,4.2);
    
    test<int>();
}
