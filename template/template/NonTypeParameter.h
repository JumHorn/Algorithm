//
//  NonTypeParameter.h
//  template
//
//  Created by Jum on 2017/11/13.
//  Copyright © 2017年 Jum. All rights reserved.
//

#ifndef NonTypeParameter_H
#define NonTypeParameter_H

#include <iostream>
using namespace std;

//template<typename T, double MAXSIZE>  //A non-type template parameter cannot have type double
template<typename T,int MAXSIZE>
class Animal
{
public:
    Animal()
    {
        cout<<MAXSIZE<<endl;
    }
};

#endif /* NonTypeParameter_H */
