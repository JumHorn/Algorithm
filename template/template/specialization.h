//
//  specialization.h
//  template
//
//  Created by Jum on 2017/11/13.
//  Copyright © 2017年 Jum. All rights reserved.
//

#ifndef specialization_H
#define specialization_H

#include <iostream>
using namespace std;

template<typename T>
class Animal
{
public:
    Animal()
    {
        cout<<"Animal"<<endl;
    }
};

//这是上面的模板的特化版本
template<typename T>
class Animal<T*>
{
public:
    Animal()
    {
        cout<<"specilization"<<endl;
    }
};

#endif /* specialization_H */
