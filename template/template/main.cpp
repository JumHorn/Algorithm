//
//  main.cpp
//  FirstMac
//
//  Created by Jum on 2017/8/19.
//  Copyright © 2017年 Jum. All rights reserved.
//

#include <iostream>
#include <vector>
#include "print.h"
using namespace std;

int main() {
    int array[] = {1,2,3,4,5};
    vector<int> vec(array,array+5);
    print(vec);
    return 0;
}
