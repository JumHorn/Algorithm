//
//  ReturnTemplateType.h
//  template
//
//  Created by Jum on 2017/11/12.
//  Copyright © 2017年 Jum. All rights reserved.
//

#ifndef ReturnTemplateType_H
#define ReturnTemplateType_H

#include <iostream>
using namespace std;

template <typename T>
T const& test(T &a,T &b)
{
    cout<<"T const& test(T &a,T &b)"<<endl;
    return a>b?a:b;
}

template<typename T1,typename T2,typename RT>
//RT test(T1 &a,T2 &b)  //引用作为参数，在参数实际传递时，不能传值，test<int,double,double>(4,4.2);
RT test(T1 a,T2 b)
{
    RT rt;
    cout<<"RT const& test(T1 &a,T2 &b)"<<endl;
    return rt;
}

template<typename RT,typename T1,typename T2>
RT test(T1 a,T2 b)
{
    RT rt;
    cout<<"RT const& test<>(T1 &a,T2 &b)"<<endl;
    return rt;
}

//没有参数，与上面的形成函数重载
template<typename T>
void test()
{
    T t;
    cout<<"void test()"<<endl;
}

#endif /* ReturnTemplateType_H */
