//
//  print.cpp
//  FirstMac
//
//  Created by Jum on 2017/8/20.
//  Copyright © 2017年 Jum. All rights reserved.
//
#include<iostream>
#include "print.h"
using namespace std;
template<typename T>
void print(T& vec)
{
    for(typename T::iterator iter =vec.begin();iter!=vec.end();iter++)
    {
        cout<<*iter<<",";
    }
//    for(int i=0;i<1;i++)
//    {
//        cout<<vec[i]<<",";
//    }
    cout<<endl;
}

//模板分离使用需要声明
//template void print<vector<int> >(vector<int>& vec); //correct
template void print(vector<int>& vec);

//example
//template<typename T>
//void print(vector<T> &vec)
//{
//    for(typename vector<T>::iterator iter=vec.begin();iter!=vec.end();iter++)
//    {
//        cout<<*iter<<",";
//    }
//}
