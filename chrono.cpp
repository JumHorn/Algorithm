#include<iostream>
#include<chrono>
#include<iomanip>// for put_time
using namespace std;

int main()
{
	time_t timenow=chrono::system_clock::to_time_t(chrono::system_clock::now());
	cout<<ctime(&timenow)<<endl;//a default format
	tm* t=localtime(&timenow);//Local time
	//tm* t=gmtime(&timenow);//UTC time
	//put_time(gmtime(&timenow),"%c %Z");
	cout<<t->tm_year+1900<<"-"<<t->tm_mon+1<<"-"<<t->tm_mday<<endl;
	cout<<t->tm_hour<<":"<<t->tm_min<<":"<<t->tm_sec<<endl;

	//steady_clock used for measuring intervals
	chrono::steady_clock::time_point start = chrono::steady_clock::now();
    cout << "Hello World\n";
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    cout << "Printing took "
              << chrono::duration_cast<chrono::microseconds>(end - start).count()
              << "us.\n";
}
