#include<iostream>
using namespace std;

struct LinkedList
{
	int val;
	LinkedList* next;
	LinkedList(int x):val(x),next(NULL){}
};


LinkedList* reverse(LinkedList* head)
{
	if(head==NULL||head->next==NULL)
	{
		return head;
	}
	LinkedList* tmp=reverse(head->next);
	head->next->next=head;
	head->next=NULL;
	return tmp;
}

LinkedList* create()
{
	LinkedList* head=new LinkedList(0);
	LinkedList* tmp=head;
	for(int i=0;i<9;i++)
	{
		tmp->next=new LinkedList(i+1);
		tmp=tmp->next;
	}
	return head;
}

ostream& operator<<(ostream& os,LinkedList* head)
{
	while(head!=NULL)
	{
		os<<head->val<<" ";
		head=head->next;
	}
	return os<<endl;
}

int main()
{
	LinkedList* tmp=create();
	cout<<tmp;
	cout<<reverse(tmp);
	return 0;
}
