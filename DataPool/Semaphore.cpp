#include "Semaphore.h"

Semaphore::Semaphore(int permits) :resource(permits)
{
}

Semaphore::~Semaphore()
{
}

void Semaphore::wait(int permits)
{
	unique_lock<mutex> lock(m);
	while (permits > resource)
	{
		cv.wait(lock);//wait将线程加入到等待队列，并释放lock占用的mutex
	}
	resource -= permits;
	cv.notify_one();
}

void Semaphore::post(int permits)
{
	unique_lock<mutex> lock(m);
	resource += permits;
	cv.notify_one();
}
