#include "ConcurrencyPool.h"
#include <boost/thread.hpp>
#include <iostream>
using namespace boost;

DataPool pool(8);
void push()
{
	static int c = 0;
	for (int i = 0; i < 4000; i++)
	{
		char *a = "123";
		pool.pushData(a, 3);
		std::cout << "push:" << ++c;
	}
}

void pop()
{
	static int c = 0;
	for (int i = 0; i < 4000; i++)
	{
		char *a = new char[4];
		int len;
		pool.popData(a, len);
		std::cout << "pop:" << ++c;
	}
}

int main()
{
	thread t1(push);
	thread t2(pop);
	thread t3(push);
	thread t4(pop);
	thread t5(push);
	thread t6(pop);
	thread t7(push);
	thread t8(pop);
	thread t9(push);
	thread t10(pop);
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();
	t10.join();
}