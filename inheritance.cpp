#include<iostream>
using namespace std;

class Animal
{
public:
	virtual void bark() const
	{
		cout<<"wa"<<endl;
	}
};

class Dog : public Animal
{
public:
	void bark() const
	{
		cout<<"wang"<<endl;
	}
	int height;
};

class Cat : protected Animal
{
public:
	void bark()
	{
		cout<<"miao"<<endl;
	}
};

//this call animal bark
//void bark(const Animal a)
//{
//	a.bark();
//}

//this call Dog bark
void bark(Dog* a)
{
	a->bark();
	a->height=3; //write to unreadable address corruption
	cout<<a->height<<endl;
}

int main()
{
	//on windows
	//debug version the height may be a random value in the address
	//release version write to the address of height cause corruption
	//but do not cause any mistake in GCC with differece optimizations
	Animal *a = new Animal();
	bark((Dog*)a);
	
	Cat c;
	c.bark();
	delete a;
	a=new Dog();
	delete a;
	a->bark();
	//a=new Cat(); //cannot cast 'Cat' to its protected base class 'Animal'
}
