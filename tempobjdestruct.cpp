#include<iostream>
using namespace std;

class A
{
public:
    A()
    {
        cout<<"construct A"<<endl;
    }
    A(const A& a)
    {
        cout<<"copy A"<<endl;
    }
    A& operator=(const A& a)
    {
        cout<<"assign A"<<endl;
        return *this;
    }
    ~A()
    {
        cout<<"destruct A"<<endl;
    }
};

A geta()
{
    // A *a;
    // return *a;    //这种实现是调用copy A
    A a;
    return a;
}

int main()
{
    A a = geta(); //编译器优化，将a直接作为temporary变量

    // A b;
    // A a = b; //copy a

    // A a;
    // a = geta();  //assign a
}