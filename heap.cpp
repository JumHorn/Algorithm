/*
 * max heap implementation
 */
#include<iostream>
using namespace std;

template<typename T>
class heap
{
private:
	T *data;
	unsigned int size;
	unsigned int end;//the index of the last element
public:
	heap(unsigned int size=0);
	~heap();
	
	void push(const T& t);
	T pop();
};

template<typename T>
heap<T>::heap(unsigned int size)
{
	data=new T[size+1];	
	this->size=size+1;
	end=0;
}

template<typename T>
heap<T>::~heap()
{
	if(data!=NULL)
	{
		delete[] data;
	}
}

template<typename T>
void heap<T>::push(const T& t)
{
	// out of bounds
	int index=end+1; //zero index reserved
	if(index>size)
	{
		return;
	}
	data[index]=t;
	while(index>1)
	{
		if(t>data[index/2])
		{
			data[index]=data[index/2];
			data[index/2]=t;
		}
		else
		{
			break;
		}
		index=index/2;
	}	
	end++;
}

template<typename T>
T heap<T>::pop()
{
	// empty heap
	if(end==0)
	{
		return T();
	}
	data[0]=data[1];
	data[1]=data[end];
	int index=1;
	int maxnode=index*2+1;
	while(maxnode<=end)
	{
		if(data[maxnode]<data[maxnode-1])
			maxnode--;
		if(data[maxnode]>data[end])
		{
			data[maxnode/2]=data[maxnode];
			data[maxnode]=data[end];
		}
		else
		{
			break;
		}
		maxnode=2*maxnode+1;
	}
	//only left node
	if(maxnode-1==end&&data[maxnode-1]>data[maxnode/2])
	{
		data[maxnode/2]=data[maxnode];
		data[maxnode-1]=data[end];
	}
	end--;
	return data[0];
}

int main()
{
	heap<int> h(100);
	for(int i=0;i<5;i++)
	{
		h.push(i);
	}
	for(int i=0;i<5;i++)
	{
		cout<<h.pop()<<endl;
	}
	return 0;
}
