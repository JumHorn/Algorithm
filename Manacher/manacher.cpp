#include<iostream>
#include<sstream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;

int getPalindromic(const string& str)
{
	//preprocess
	//abbac	=>	$#a#b#b#a#c#\0
	stringstream ss;
	ss << '$';
	for (string::size_type i = 0; i < str.length(); i++)
	{
		ss << '#' << str[i];
	}
	ss << '#' << '\0';
	string newstr = ss.str();

	//calculate a radius arrary
	vector<unsigned int> Radius(newstr.length(), 0);
	unsigned int center = 1;//the center of the most right radius
	unsigned int maxradius=1;//the pointer of the most right raius

	for (string::size_type i = 2; i < newstr.length() - 1; i++)
	{
		if (i < maxradius)
			Radius[i] = min(Radius[2 * center - i], maxradius - i);
		else
			Radius[i] = 1;

		while (newstr[i + Radius[i]] == newstr[i - Radius[i]])//with added $ and \0.this equation will never be out of range
			Radius[i]++;

		if (Radius[i] + i>maxradius)//change center and the maxradius
		{
			maxradius = Radius[i] + i;
			center = i;
		}
	}

	return (int)*max_element(Radius.begin(), Radius.end())-1;
}

int main()
{
	cout << getPalindromic("abccbacbbcab") << endl;
}