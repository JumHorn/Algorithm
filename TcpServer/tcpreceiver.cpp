#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread/mutex.hpp>
#include <iostream>
#include <fstream>
using namespace boost::asio;
using namespace boost::asio::ip;

io_context netio;
tcp::acceptor acceptor(netio, tcp::endpoint(tcp::v4(), 8192));
tcp::socket recvsocket(netio);
boost::array<char, 15> recvbuf;
boost::mutex mfree;

void handleAccept(boost::system::error_code)
{
}

void handleClientRead(const boost::system::error_code & error, std::size_t bytes_transferred)
{
	std::cout << recvbuf.data() << std::endl;
	recvsocket.async_read_some(buffer(recvbuf), handleClientRead);
}

int main()
{
	try
	{
		acceptor.accept(recvsocket, handleAccept);
		recvsocket.async_read_some(buffer(recvbuf), handleClientRead);
		netio.run();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}