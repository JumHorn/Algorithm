#include<iostream>
#include<vector>
using namespace std;

void conquer(vector<int>& tmp,vector<int>::iterator start,vector<int>::iterator mid,vector<int>::iterator end)
{
	vector<int>::iterator s=start,m=mid,d=tmp.begin(),d1=d;
	while(s!=mid&&m!=end)(*s>*m)?(*d++=*m++):(*d++=*s++);
	while(s!=mid)*d++=*s++;
	while(m!=end)*d++=*m++;
	while(start!=end)*start++=*d1++;
}

void divide(vector<int>& tmp,vector<int>::iterator start,vector<int>::iterator end)
{
	if(distance(start,end)<=1)
	{
		return;
	}
	int mid=distance(start,end)/2;
	divide(tmp,start,start+mid);
	divide(tmp,start+mid,end);
	conquer(tmp,start,start+mid,end);
}

void mergeSort(vector<int>::iterator start,vector<int>::iterator end)
{
	vector<int> tmp(distance(start,end));
	divide(tmp,start,end);
}

ostream& operator<<(ostream& os,vector<int>& v)
{
	for(vector<int>::iterator iter=v.begin();iter!=v.end();iter++)
	{
		os<<*iter<<" ";
	}
	return os<<endl;
}

int main()
{
	char arr[]={9,8,7,6,5,4,3,2,1,0};
	vector<int> v(arr,arr+10);
	mergeSort(v.begin(),v.end());
	cout<<v;
	return 0;
}
