#include<iostream>
#include<vector>
#include<algorithm> // for swap
#include<chrono>
using namespace std;

ostream& operator<<(ostream& os, vector<int>& v)
{
	for (vector<int>::size_type i = 0; i < v.size(); i++)
		os << v[i];
	return os << endl;
}

//指定位数的排列形式
void permutation(vector<int>& v, vector<int>& tmp, int size, int start)
{
	if (tmp.size() == size)
	{
		cout << tmp;
		return;
	}
	for (vector<int>::size_type i = start; i < v.size(); i++)
	{
		swap(v[start], v[i]);
		tmp.push_back(v[start]);
		permutation(v, tmp, size, start + 1);
		tmp.pop_back();
		swap(v[start], v[i]);
	}
}

//全排列形式显示
void permutation(vector<int>& v, vector<int>& tmp, int start)
{
	cout << tmp;
	for (vector<int>::size_type i = start; i < v.size(); i++)
	{
		swap(v[start], v[i]);
		tmp.push_back(v[start]);
		permutation(v, tmp, start + 1);
		tmp.pop_back();
		swap(v[start], v[i]);
	}
}

int main()
{
	vector<int> v = { 1,2,3,4 };
	vector<int> tmp;
	//permutation(v, tmp, 2, 0);
	chrono::steady_clock::time_point start = chrono::steady_clock::now();
	permutation(v, tmp, 0);
	chrono::steady_clock::time_point end = chrono::steady_clock::now();

	cout << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms" << endl;
	return 0;
}