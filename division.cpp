#include<iostream>
using namespace std;

//减法代替除法的进阶版
int division(long dividend, long divisor)
{
	long quotient=0,temp=1;
	int symbol=1,i=0;
	long d1=dividend,d2=divisor;
	if(dividend<0)
	{
		d1=-d1;
		symbol *= -1;
	}
	if(divisor<0)
	{
		d2=-d2;
		symbol *= -1;
	}

	while(true)
	{
		if(d1-d2*(temp<<i)==0)
		{
			quotient += temp<<i;
			break;
		}
		else if(d1-d2*(temp<<i)<0)
		{
			quotient += temp<<(i-1);
			d1 -= d2*(temp<<(i-1));
			i=0;
		}
		else
		{
			i++;
		}
	}
	return symbol*quotient;
}

int main()
{
	cout<<division(-6442450944,-2147483648)<<endl;
}
