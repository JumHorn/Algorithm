#include<iostream>
using namespace std;

int backtracing[8];
int total = 0;

void find(int m, int n)
{
	backtracing[m] = n;
	if (m == 0 && n == 8)
	{
		return;
	}
	if (n == 8)
	{
		return find(m - 1, backtracing[m - 1] + 1);
	}

	//vertical check
	for (int i = 0; i < m; i++)
	{
		if (backtracing[i] == n)
		{
			return find(m, n + 1);
		}
	}

	//slant check
	int t;
	//check left
	t = m > n ? n : m;
	for (int j = 0; j < t; j++)
	{
		if (backtracing[m - j - 1] == n - j - 1)
		{
			return find(m, n + 1);
		}
	}

	//check right
	t = m > 7 - n ? 7 - n : m;
	for (int j = 0; j < t; j++)
	{
		if (backtracing[m - j - 1] == n + j + 1)
		{
			return find(m, n + 1);
		}
	}

	//success or not
	if (m == 7)
	{
		cout << total++ << endl;
		return find(m - 1, backtracing[m - 1] + 1);
	}
	else
	{
		return find(m + 1, 0);
	}
}

int main()
{
	memset((void*)backtracing, 0, sizeof(backtracing));
	find(0, 0);
}
