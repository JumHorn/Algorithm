#include<iostream>
#include<vector>
using namespace std;

void quickSort(vector<int>::iterator start,vector<int>::iterator end)
{
	if(distance(start,end)<=1)
		return;
	vector<int>::iterator left=start,right=end-1;
	int mid=*start;
	while(distance(left,right)>0)
	{
		while(distance(left,right)>0&&*right>=mid)right--;
		*left=*right;
		while(distance(left,right)>0&&*left<=mid)left++;
		*right=*left;
	}
	*left=mid;
	quickSort(start,left);
	quickSort(left+1,end);
}

ostream& operator<<(ostream& os,vector<int>& v)
{
	for(vector<int>::iterator iter=v.begin();iter!=v.end();iter++)
	{
		os<<*iter<<" ";
	}
	return os<<endl;
}

int main()
{
	char arr[]={9,8,7,6,5,4,3,2,1,0};
	vector<int> v(arr,arr+10);
	quickSort(v.begin(),v.end());
	cout<<v;
	return 0;
}
